package topology;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import static utility.Lamport.t;
import static utility.Printer.print_err;
import static utility.MyLogger.log;

public class WatchThread extends Thread
{
    private boolean running = true;
    private Node node;

    public WatchThread(Node node) {
        super();
        this.node = node;
    }

    public void run()
    {
        String address = "";

        try {
            address = node.getNextAddr();
        } catch (RemoteException e) {
            return;
        } catch (NullPointerException e)
        {
            print_err("Watcher: Next node unreachable");
        }

        log("Watcher: watching " + address);

        try
        {
            while(running)
            {
                Thread.sleep(1000);
                node.getServiceNext().statusMSG(t());
            }
        }
        catch (Exception e)
        {
            try
            {
                node.getServicePrev().sendBrokenTopologyMSG(node.getAddress(), t());
                log("Watcher: Sending broken topology message with missing address: " + address);
            }
            catch (NotBoundException | IOException e1)
            {
                log("Watcher: Lost connection with prev node of " + node.getAddress() +
                        " while sending broken topology message");
            }
        }
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}
