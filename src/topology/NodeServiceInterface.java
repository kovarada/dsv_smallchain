package topology;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface NodeServiceInterface extends Remote
{
    String getAddressMSG(int timeStamp) throws RemoteException;
    String statusMSG(int timeStamp) throws RemoteException;
    boolean mergeMSG(String senderAddr, String senderPrevAddr, int timeStamp) throws RemoteException, MalformedURLException, NotBoundException;
    void registerNextAddr(String addr, int timeStamp) throws RemoteException, MalformedURLException, NotBoundException;
    void registerPrevAddr(String addr, int timeStamp) throws RemoteException, MalformedURLException, NotBoundException;
    void sendBrokenTopologyMSG(String missingAddr, int timeStamp) throws RemoteException, MalformedURLException, NotBoundException;
    void sendNewNodeMSG(String reporterAddr, String newAddr, int timeStamp) throws RemoteException, MalformedURLException, NotBoundException;
    boolean checkPresentAddr(String reporterAddr, String addr) throws RemoteException;
    String getNodeId(int timeStamp) throws RemoteException;
    String getNextAddr() throws RemoteException;
    String echo(String senderId, String msg) throws RemoteException;
}
