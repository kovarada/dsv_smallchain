package topology;

import blockchain.User;
import service.ServiceInterface;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.ConnectException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Objects;

import static utility.Lamport.t;
import static utility.Lamport.incrementClock;
import static utility.Printer.println;
import static utility.MyLogger.log;

public class Node implements Serializable
{
    private final String address;
    private final User user;
    private ServiceInterface servicePrev = null;
    private ServiceInterface serviceNext = null;
    private WatchThread watcher = null;

    public Node(String address)
    {
        this.address = address;
        user = new User();
    }

    public void setServiceNext(String nextAddr) throws RemoteException, NotBoundException, MalformedURLException
    {
//        incrementClock();
        if(watcher != null) stopWatcher();
        serviceNext = (ServiceInterface) Naming.lookup(nextAddr);
        println("T: " +serviceNext.getNodeId(t()) + " at: " + nextAddr + " set as NEXT");
        watcher = new WatchThread(this);
        watcher.start();
    }

    public void setServicePrev(String prevAddr) throws RemoteException, NotBoundException, MalformedURLException
    {
        incrementClock();
        servicePrev = (ServiceInterface) Naming.lookup(prevAddr);
        println("T: "  + servicePrev.getNodeId(t()) + " at: " + prevAddr + " set as PREV");
    }

    public void stopWatcher()
    {
        incrementClock();
        if (watcher != null)
        {
            watcher.setRunning(false);
            watcher = null;
        }
    }

    public void disconnect() throws IOException, NotBoundException
    {
        incrementClock();
        if (watcher != null) stopWatcher();
        try
        {
            if (!servicePrev.getAddressMSG(t()).equals(serviceNext.getAddressMSG(t())))
            {
                servicePrev.registerNextAddr(getNextAddr(), t());
                serviceNext.registerPrevAddr(getPrevAddr(), t());
            }
        }catch (ConnectException | NullPointerException e) {}
        log("T: Node " + user.getId() + " at: " + address + " disconnected");
    }

    public ServiceInterface getServicePrev() { return servicePrev; }

    public ServiceInterface getServiceNext() { return serviceNext; }

    public String getAddress() { return address; }

    public String getPrevAddr() throws RemoteException { return servicePrev.getAddressMSG(t()); }

    public String getNextAddr() throws RemoteException { return serviceNext.getAddressMSG(t()); }

    public String getId() { return user.getId(); }

    public User getUser() { return user; }

    @Override
    public int hashCode() { return Objects.hash(address); }

}
