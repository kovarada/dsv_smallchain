package blockchain;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

import static utility.Printer.*;
import static utility.Lamport.*;

public class User
{
    private String id;
    private boolean miner = true;
    private boolean active = false;
    private int lastResult = Integer.MAX_VALUE;
    private Block pendingBlock = null;
    private Chain chain = null;
    private int balance;
    private MinerThread minerThread;

    Random rand = new Random();

    public User()
    {
        this.id = "none";
    }

    public void doWork() throws InterruptedException
    {
        incrementClock();
        setActive(true);
        int duration = rand.nextInt(6) + 2;
        minerThread = new MinerThread(this, duration);
        println("Mining for " + duration + " seconds");
        minerThread.start();
        lastResult = duration;
    }

    public void createChain()
    {
        incrementClock();
        chain = new Chain(new ArrayList<>());
        chain.add(new Block(0,new Transaction("Xenu", id, 10), id));
        addToBalance(10);
        println("B: Created new genesis block");
        try {
            saveChain();
        }
        catch (IOException e) { e.printStackTrace(); }
    }

    public void loadChain() throws IOException, ClassNotFoundException
    {
        FileInputStream file = new FileInputStream("data\\chain" + id + ".ser");
        ObjectInputStream in = new ObjectInputStream(file);

        // Method for deserialization of object
        chain = (Chain) in.readObject();

        in.close();
        file.close();
        println("B: Local chain loaded successfully");
    }

    public synchronized void saveChain() throws IOException
    {
        FileOutputStream file = null;
        try{ file = new FileOutputStream("data\\chain" + id + ".ser"); }
        catch (FileNotFoundException e)
        {
            new File("data").mkdir();
            file = new FileOutputStream("data\\chain" + id + ".ser");
        }
        ObjectOutputStream out = new ObjectOutputStream(file);
        out.writeObject(chain);
        out.close();
        file.close();
    }

    public void addBlock(Block block)
    {
        incrementClock();
        chain.add(block);
        println("B: New block added");
        if(block.getTransaction().getFrom().equals(id)) reduceBalance(block.getTransaction().getValue());
        if(block.getTransaction().getTo().equals(id)) addToBalance(block.getTransaction().getValue());
        if(block.getSolverId().equals(id))
        {
            println("B: Recieved " + printCurrency(1) + " from mining");
            balance += 1;
        }
    }

    public void collectBalanceFromChain()
    {
        balance = 0;
        if(chain == null) return;
        else
        {
            balance += chain.getData().stream()
                    .filter(block -> block.
                            getTransaction()
                            .getTo().
                                    equals(id))
                    .mapToInt(block -> block.getTransaction().getValue())
                    .reduce(0, Integer::sum);
            balance -= chain.getData().stream()
                    .filter(block -> block.getTransaction().getFrom().equals(id))
                    .mapToInt(block -> block.getTransaction().getValue())
                    .reduce(0, Integer::sum);
            balance += chain.getData().stream()
                    .filter(block -> block.getSolverId().equals(id))
                    .mapToInt(block -> 1).reduce(0, Integer::sum);
        }
    }

    public void addToBalance(int value)
    {
        incrementClock();
        balance += value;
        println("B: Recieved " + printCurrency(value));
    }

    public void reduceBalance(int value)
    {
        incrementClock();
        balance -= value;
        println("B: Sent " + printCurrency(value));
    }

    public String getId() { return id; }

    public boolean isMiner() { return miner; }

    public void setMiner(boolean miner) { this.miner = miner; }

    public boolean isActive() { return active; }

    public void setActive(boolean active) { this.active = active; }

    public int getLastResult() { return lastResult; }

    public Block getPendingBlock() { return pendingBlock; }

    public void setPendingBlock(Block pendingBlock) { this.pendingBlock = pendingBlock; }

    public int getLastHash()
    {
        if(chain == null) return 0;
        return chain.get(chain.size() - 1).hashCode();
    }

    public Chain getChain() { return chain; }

    public void setChain(Chain chain)
    {
        incrementClock();
        if(chain != null)
        {
            this.chain = chain;
            collectBalanceFromChain();
            println("B: New local chain set");
            try
            {
                saveChain();
            }
            catch (IOException e) { e.printStackTrace(); }
        }
        else println("B: Trying to set invalid chain");
    }

    public int getBalance() { return balance; }

    public void setId(String id) { this.id = id; }
}

