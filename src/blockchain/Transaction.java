package blockchain;

import java.io.Serializable;

public class Transaction implements Serializable {

    private final String from, to;
    private final int value;

    public Transaction(String from, String to, int value) {
        this.from = from;
        this.to = to;
        this.value = value;
    }

    public String getFrom() {return from; }

    public String getTo() {return to; }

    public int getValue() {return value; }
}
