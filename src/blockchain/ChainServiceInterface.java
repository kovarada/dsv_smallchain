package blockchain;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ChainServiceInterface extends Remote
{
    void publishTransactionMSG(Transaction transaction, int timeStamp) throws RemoteException;
    void publishBlockMSG(Block block, int timeStamp) throws RemoteException;
    void sendWorkDoneMSG(String senderId, String solverId, int duration, int timeStamp) throws RemoteException;
    boolean askForCurrentChainMSG(String senderId, int timeStamp) throws RemoteException;
    boolean askForCurrentChainMSG(Chain chain, String senderId, int timeStamp) throws RemoteException;
    boolean checkTerminationMSG(String senderID, int timeStamp) throws RemoteException;
    boolean checkValidIdMSG(String senderID) throws RemoteException;
    void findWinnerMSG(String senderID, String solverId, int duration, int timeStamp) throws RemoteException;
    void setChain(Chain chain, int timeStamp) throws RemoteException;
    Chain getChain(int timeStamp) throws RemoteException;

}
