package blockchain;

import java.io.Serializable;
import java.util.Objects;

public class Block implements Serializable {

    private final int prevHash;
    private final Transaction transaction;
    private String solverId;

    public Block (int prevHash, Transaction transaction)
    {
        this.prevHash = prevHash;
        this.transaction = transaction;
    }

    public Block(int prevHash, Transaction transaction, String solverId)
    {
        this.prevHash = prevHash;
        this.transaction = transaction;
        this.solverId = solverId;
    }

    public void setSolverId(String solverId) { this.solverId = solverId; }

    public String getSolverId() { return solverId; }

    public Transaction getTransaction() { return transaction; }

    @Override
    public int hashCode() {
        return Objects.hash(prevHash, transaction, solverId);
    }
}
