package blockchain;

import java.util.ArrayList;

public class Chain implements java.io.Serializable
{
    private ArrayList<Block> data = null;

    Chain(ArrayList<Block> data) { this.data = data; }

    public ArrayList<Block> getData() { return data; }

    void add(Block block) { data.add(block); }

    Block get(int index) { return data.get(index);}

    public int size() {return data.size(); }

    public int getLastHash()
    {
        if(data == null) return 0;
        return data.get(size() - 1).hashCode();
    }
}
