package blockchain;

import static utility.Lamport.t;
import static utility.MyLogger.log;
import static utility.Printer.*;

public class MinerThread extends Thread {

    private User user;
    private final int duration;

    public MinerThread(User user, int duration) {
        super();
        this.user = user;
        this.duration = duration;
    }

    public void run()
    {
        log("B: User " + user.getId() + " is mining for " + duration + " seconds\nLt: " + t());
        for (int i = 0; i < duration; i++)
        {
            try
            {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        user.setActive(false);
    }

}
