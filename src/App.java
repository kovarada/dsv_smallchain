import blockchain.Block;
import blockchain.Transaction;
import service.Service;
import service.ServiceInterface;
import topology.Node;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.util.ConcurrentModificationException;
import java.util.Scanner;

import static utility.Lamport.t;
import static utility.Printer.*;
import static utility.MyLogger.*;
import static utility.Printer.println;

public class App
{
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException, NotBoundException
    {
        printLogo();
        String ans = "";
        String port = getArgParam(args, "-p");
        String id = getArgParam(args, "-id");
        String thisAddress = getArgParam(args, "-ip");
        String netAddress = getArgParam(args, "-a");
        if(port.equals(""))
        {
            port = ask("Enter port number: ");
        }

        boolean validPort = false;
        Registry registry = null;

        while(!validPort)
        {
            try { registry = LocateRegistry.createRegistry(Integer.parseInt(port)); validPort = true; }
            catch(ExportException | NumberFormatException e)
            {
                String msg = (e instanceof  NumberFormatException) ?
                        "T: Wrong port number format, enter different port number: " :
                        "T: Address already in use, enter different port number: ";
                port = ask(msg);
            }
        }

        if(id.equals("")) id = ask("Enter ID: ");

        boolean isT = has(args, "-t");
        boolean isD = has(args, "-d");

        if(thisAddress.equals("") && !isT && !isD)
        {
            ans = ask("Use default host ip address? [yes/no]: ");
            if(isNo(ans))
            {
                thisAddress = ask("Enter IP address: ");
            }
            else thisAddress = java.net.InetAddress.getLocalHost().getHostAddress();
        }
        else thisAddress = (isT) ? "localhost" : (isD) ? java.net.InetAddress.getLocalHost().getHostAddress() : thisAddress;

        System.setProperty("java.rmi.server.hostname", thisAddress);

        Node node = new Node("rmi://" + thisAddress + ":" + port + "/service");
        registry.rebind("service", new Service(node));

        println("T: Registry running on: " + node.getAddress());

        if(!netAddress.equals(""))
        {
            try
            {
                node.setServicePrev("rmi://" + netAddress + "/service");
                node.getServicePrev().sendNewNodeMSG(node.getPrevAddr(), node.getAddress(), t());
                while(!node.getServicePrev().checkValidIdMSG(id))
                {
                    id = ask("ID " + id + " is already taken, enter a different one: ");
                }
            }
            catch(ConnectException | UnknownHostException | MalformedURLException | ConnectIOException | NotBoundException e)
            {
                node.getUser().setId(id);
                println("T: Connection refused to host: " + netAddress);
                ans = ask("T: Enter another address? [yes/no]: ");
                while (isYes(ans))
                {
                    ans = ask("Enter IP address: ");
                    try
                    {
                        node.getUser().setId("none");
                        node.setServicePrev("rmi://" + ans + "/service");
                        node.getServicePrev().sendNewNodeMSG(node.getPrevAddr(), node.getAddress(), t());
                        while(!node.getServicePrev().checkValidIdMSG(id))
                        {
                            id = ask("ID " + id + " is already taken, enter a different one: ");
                        }
                        node.getUser().setId(id);
                        break;
                    }
                    catch(ConnectException | UnknownHostException | MalformedURLException | ConnectIOException ex)
                    {
                        ans = ask("T: Connection refused to host: " + ans + "\nEnter another address? [yes/no]: ");
                    }
                }
                if (isNo(ans))
                {
                    if (isNo(ask("T: Create new topology? [yes/no]: "))) exit(0);
                }

            }
        }

        node.getUser().setId(id);
        println("Application is running");

        try
        {
            node.getUser().loadChain();
            node.getUser().collectBalanceFromChain();
        } catch (ClassNotFoundException | IOException e)
        {
            println("B: Local chain not available");
        }

        processBlockChainCommands("ask", node);

        while (!isExit(ans))
        {
            ans = scan.next().trim();
            boolean topologyAns = processTopologyCommands(ans, node);
            boolean chainAns = processBlockChainCommands(ans, node);
            boolean isHelp = printUsage(ans);
            if(!(topologyAns || chainAns || isExit(ans) || isHelp)) println("Invalid command");
        }
        node.disconnect();
        node.getUser().saveChain();
        exit(0);
    }

    private static boolean processTopologyCommands(String command, Node node)
    {
        if (isVerb(command))
        {
            println("T: Verbose mode " + (getVerbose() ? "off" : "on"));
            setVerbose(!getVerbose());
            return true;
        }
        try
        {
            if (command.equals("n") || command.equals("p"))
            {
                println(command.equals("n") ? "T: Next: " + node.getNextAddr() : command.equals("p") ? "T: Prev: " + node.getPrevAddr() : "");
                return true;
            }
        }
        catch(NullPointerException | RemoteException e)
        {
            println("T: Node " + (command.equals("n") ? "NEXT" : "PREV") + " is unreachable");
            return true;
        }
        if(command.equals("t"))
        {
            println("T: Current logical time is: " + t());
            return true;
        }
        if(command.equals("m") || command.equals("merge"))
        {
            if(node.getServicePrev() == null)
            {
                println("T: Cannot merge if not connected.");
                return true;
            }
            String addr = "rmi://" + ask("(BETA) Enter address of node in the topology to merge with: ") + "/service";
            try
            {
                ServiceInterface temp = (ServiceInterface) Naming.lookup(addr);
                if(temp.checkPresentAddr(node.getAddress(), addr)) println("T: Cannot merge with already connected node.");
                else
                {
                    String prevAddr = node.getPrevAddr();
                    node.setServicePrev(addr);
                    node.getServicePrev().mergeMSG(node.getAddress(), prevAddr, t());
                    node.getServicePrev().askForCurrentChainMSG(node.getId(), t());

                }
            }
            catch(RemoteException | NotBoundException | MalformedURLException e)
            {
                println("T: Connection refused to host: " + addr + "\nMerge aborted. ");
            }
            return true;
        }
        if(command.equals("echo"))
        {
            if(node.getServicePrev() == null) println("T: Not connected to any node. ");
            else
            {
                try {
                    println("T: Current network:\n   " +
                            node.getServicePrev().echo(node.getId(), node.getId())
                    + " - " + node.getId());
                } catch (Exception e) {
                    println("T: Not connected to any node. ");
                }
            }
            return true;
        }
        return false;
    }


    private static boolean processBlockChainCommands(String ans, Node node) throws IOException
    {
        if(ans.equals("ask") || ans.equals("a"))
        {
            try
            {
                println("B: Asking " + node.getServicePrev().getNodeId(t()) + " for current chain");
                if(!node.getServicePrev().askForCurrentChainMSG(node.getUser().getChain(), node.getId(), t()))
                {
                    println("B: Local chain up to date");
                } else node.getUser().collectBalanceFromChain();
            } catch (RemoteException | NullPointerException e)
            {
                print("B: Remote chain is unreachable");
                if(node.getUser().getChain() == null)
                {
                    ans = ask(" and local chain is empty, create genesis block? [yes/no] ");
                    if (isYes(ans))
                    {
                        node.getUser().createChain();
                        node.getUser().saveChain();
                    }
                    else println("B: Not creating local chain");
                } else println("");
            }
            return true;
        }
        if(ans.equals("new"))
        {
            try
            {
                println("B: Syncing with remote chain...");
                if(!node.getServicePrev().askForCurrentChainMSG(node.getUser().getChain(), node.getId(), t()))
                {
                    println("B: Local chain up to date");
                }
            }
            catch (RemoteException | NullPointerException e)
            {
                println("B: Remote chain is unreachable, transaction aborted");
                return true;
            }
            String to = ask("B: Creating new transaction:\nTo: ");
            while(to.equals(node.getId()))
            {
                to = ask("To: ");
            }
            print("Value: ");
            int value = 0;
            while(value <= 0)
            {
                try
                {
                    value = Integer.parseInt(scan.next().trim());
                    if(value <= 0) throw new NumberFormatException();
                }
                catch(NumberFormatException e)
                {
                    print("B: Enter value in correct format [int > 0]\nValue: ");
                    value = 0;
                }
            }
            if(value > node.getUser().getBalance())
            {
                println("B: Cannot send " + value + "! Your current balance is " + printCurrency(node.getUser().getBalance())
                + "\nTransaction aborted");
                return true;
            }

            int i = 0;

            try
            {
                while(!node.getServicePrev().checkTerminationMSG(node.getId(), 0))
                {
                    if (i == 0) print("B: Waiting for previous transaction termination to get published");
                    if (i++ % 1000 == 0) print(".");
                }
            }
            catch(Exception e) { println("T: Connection lost, try again"); return true;}
            if(i > 0) println("");
            node.getUser().setMiner(false);
            Transaction t = new Transaction(node.getId(), to, value);
            node.getServicePrev().publishTransactionMSG(t, t());
            print("B: Transaction published\nWaiting for validation");
            node.getUser().setPendingBlock(new Block(node.getUser().getChain().getLastHash(),t));
            boolean done = false;
            i = 0;
            while (!done)
            {
                try {
                    done = node.getServicePrev().checkTerminationMSG(node.getId(), 0);
                    if (i++ % 1000 == 0) print(".");
                    if(done) node.getUser().setActive(true);
                }
                catch(Exception e)
                {
                    log("T: Some node is lost. \nLt: " + t());
                    try { node.getServicePrev().statusMSG(t()); node.getServiceNext().statusMSG(t()); }
                    catch (Exception ee )
                    {
                        try { node.getServiceNext().statusMSG(t()); }
                        catch (Exception eee )
                        {
                            println("\nT: Lost connection, transaction aborted");
                            node.getUser().setPendingBlock(null);
                            return true;
                        }
                    }
                }
            }
            println("");
            try
            {
                node.getServicePrev().findWinnerMSG(node.getId(), "None", Integer.MAX_VALUE, t());
                node.getServicePrev().publishBlockMSG(node.getUser().getPendingBlock(), t());
            }
            catch(ConcurrentModificationException e) { println("B: Something went wrong during writing into local chain, asking for current remote is advised.");}
            node.getUser().setMiner(true);
            node.getUser().setPendingBlock(null);
            node.getUser().setActive(false);
            return true;
        }
        if(ans.equals("balance") || ans.equals("b"))
        {
            println("B: Your balance is: " + printCurrency(node.getUser().getBalance()));
            return true;
        }
        if(ans.equals("save") || ans.equals("s"))
        {
            println("B: Saving current local chain");
            node.getUser().saveChain();
            return true;
        }

        return false;
    }

    private static boolean printUsage(String input)
    {
        if(!(input.equals("help") || input.equals("h"))) return false;

        println(
                "\nUsage:\n\n - Topology:\n   - 'n' print addres of next node\n   - 'p' print addres of previous node\n" +
                "   - 'v' toggle logging into console output\n   - 't' print current logical time\n" +
                " - Blockchain:\n   - 'a' | 'ask' ask previous node for current remote chain\n" +
                "   - 'new' create new transaction\n   - 'b' | 'balance' print number of coins at your disposal\n" +
                "   - 's' | 'save' save current local chain\n   - 'e' | 'exit' quit application\n"
                );

        return true;
    }

    private static boolean isExit(String input) { return (input.equals("exit") || input.equals("e")) ; }
    private static boolean isVerb(String input) { return (input.equals("verb") || input.equals("v")) ; }
    private static boolean isYes(String input) { return (input.equals("yes") || input.equals("y")) ; }
    private static boolean isNo(String input) { return (input.equals("no") || input.equals("n")) ; }
    private static boolean has(String[] args, String item)
    {
        if (args.length == 0) return false;
        for(String s : args) if (s.equals(item)) return true;
        return false;
    }

    private static String getArgParam(String[] args, String param)
    {
        for (int i = 0; i < args.length; i++)
        {
            if(args[i].equals(param) && args.length - 1 > i)
            {
                return args[i + 1];
            }
        }
        return "";
    }

    private static String ask(String msg)
    {
        print(msg);
        String ans = scan.next().trim();
        if (isExit(ans)) exit(0);
        return ans;
    }

    private static void exit(int status)
    {
        println("Exiting...");
        System.exit(status);
    }

    private static void printLogo()
    {
        println(
        " ___                   _\n" +
        "|    \\                | |\n" +
        "|     |  ____   ____  | |__     _____    _____    _   ____\n" +
        "|    /  |  __| |  | | |    \\   /     |  /     \\  | | |    \\\n" +
        "|    \\  | |    |  | | |     | |  ____| |   |   | | | |  |  |\n" +
        "|     | | |    |    | |  |  | |      | |   |   | | | |  |  |\n" +
        "|____/  |_|     \\__/  |__|__|  \\_____|  \\_____/  |_| |__|__|\n" );
    }
}
