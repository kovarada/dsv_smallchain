package service;

import blockchain.ChainServiceInterface;
import topology.NodeServiceInterface;

public interface ServiceInterface extends NodeServiceInterface, ChainServiceInterface {}
