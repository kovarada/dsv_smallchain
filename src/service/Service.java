package service;

import blockchain.Block;
import blockchain.Chain;
import blockchain.Transaction;
import topology.Node;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import static utility.MyLogger.*;
import static utility.Lamport.*;
import static utility.Printer.println;

public class Service extends UnicastRemoteObject implements ServiceInterface
{
    private final Node node;

    public Service(Node node) throws RemoteException
    {
        super();
        this.node = node;
    }

    @Override
    public String getAddressMSG(int timeStamp) throws RemoteException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        return node.getAddress();
    }

    @Override
    public String statusMSG(int timeStamp) throws RemoteException
    {
        if(timeStamp > t()) setClock(timeStamp);
        return "Node " + node.getId() + " at: "+ node.getAddress() + " is active" + "\nLt: " + t();
    }

    @Override
    public boolean mergeMSG(String senderAddr, String senderPrevAddr, int timeStamp) throws RemoteException, MalformedURLException, NotBoundException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();

        if(node.getServicePrev() == null)
        {
            node.setServiceNext(senderAddr);
            node.setServicePrev(senderPrevAddr);
            node.getServicePrev().registerNextAddr(node.getAddress(), t());
        }
        else {
            node.getServiceNext().registerPrevAddr(senderPrevAddr, t());
            ServiceInterface tempNext = node.getServiceNext();
            String tempNextOfNext = node.getServiceNext().getNextAddr();
            node.getServicePrev().sendNewNodeMSG(senderPrevAddr, node.getPrevAddr(), t());
            tempNext.registerPrevAddr(senderPrevAddr, t());
            tempNext.registerNextAddr(tempNextOfNext, t());
            node.setServiceNext(senderAddr);
        }
        return true;
    }

    @Override
    public void registerNextAddr(String addr, int timeStamp) throws RemoteException, MalformedURLException, NotBoundException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        node.setServiceNext(addr);
    }

    @Override
    public void registerPrevAddr(String addr, int timeStamp) throws RemoteException, MalformedURLException, NotBoundException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        node.setServicePrev(addr);
    }

    @Override
    public void sendBrokenTopologyMSG(String reporterAddr, int timeStamp) throws RemoteException, MalformedURLException, NotBoundException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        log("T: recieved broken topology message from " + reporterAddr + "\nLt: " + t());
        try
        {
            node.getServicePrev().sendBrokenTopologyMSG(reporterAddr, t());
        }
        catch (Exception e)
        {
            if (!node.getAddress().equals(reporterAddr))
            {
                node.setServicePrev(reporterAddr);
                node.getServicePrev().registerNextAddr(node.getAddress(), t());
            }
            else
            {
                node.stopWatcher();
            }
        }
    }

    @Override
    public void sendNewNodeMSG(String reporterAddr, String newAddr, int timeStamp) throws RemoteException, MalformedURLException, NotBoundException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        log("T: recieved new node at: "+ newAddr + " message from " + reporterAddr + "\nLt: " + t());
        try
        {
            if (node.getPrevAddr().equals(reporterAddr))
            {
                node.getServicePrev().registerNextAddr(newAddr, t());
                node.setServicePrev(newAddr);
                node.getServicePrev().registerNextAddr(node.getAddress(), t());
            }
            else
            {
                node.getServicePrev().sendNewNodeMSG(reporterAddr, newAddr, t());
            }
        }catch(NullPointerException | RemoteException e)
        {
            node.setServicePrev(newAddr);
            if(node.getAddress().equals(reporterAddr)) node.setServiceNext(newAddr);
            node.getServicePrev().registerNextAddr(node.getAddress(), t());
        }
    }

    @Override
    public boolean checkPresentAddr(String reporterAddr, String addr) throws RemoteException
    {
        if (reporterAddr.equals(addr)) return true;
        if (node.getAddress().equals(reporterAddr)) return true;
        if (node.getServicePrev() == null) return false;
        if (node.getServicePrev().getAddressMSG(t()).equals(addr)) return false;
        return node.getServicePrev().checkPresentAddr(reporterAddr, addr);
    }

    @Override
    public String getNodeId(int timeStamp) throws RemoteException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        return node.getId();
    }

    @Override
    public String getNextAddr() throws RemoteException {
        return node.getNextAddr();
    }

    @Override
    public String echo(String senderId, String msg) throws RemoteException
    {
        msg += " - " + node.getId();
        println("T: Recieved echo from " + senderId);
        if(node.getServicePrev().getNodeId(t()).equals(senderId)) return msg;
        else return node.getServicePrev().echo(senderId, msg);
    }


    @Override
    public void publishTransactionMSG(Transaction transaction, int timeStamp) throws RemoteException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        log("B: recieved transaction message from " + transaction.getFrom() + "\nLt: " + t());
        if(node.getId().equals(transaction.getFrom()))
        {
            node.getUser().setPendingBlock(new Block(node.getUser().getLastHash(), transaction));
            return;
        }
        node.getServicePrev().publishTransactionMSG(transaction, t());
        try {
            node.getUser().doWork();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void publishBlockMSG(Block block, int timeStamp) throws RemoteException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        log("B: recieved publish block message from " + block.getTransaction().getFrom() + "\nLt: " + t());
        node.getUser().addBlock(block);
        try {
            node.getUser().saveChain();
        } catch (IOException e) { e.printStackTrace(); }
        if(!node.getId().equals(block.getTransaction().getFrom())) node.getServicePrev().publishBlockMSG(block, t());
    }

    @Override
    public void sendWorkDoneMSG(String senderId, String solverId, int duration, int timeStamp) throws RemoteException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        log("B: recieved work done message from " + senderId + "\nLt: " + t()
        );
        if(senderId.equals(node.getId()))
        {
            if(duration < node.getUser().getLastResult())
            {
                node.getUser().getPendingBlock().setSolverId(solverId);
            }
        }
        else
        {
            if(node.getUser().isActive())
            {
                node.getServicePrev().sendWorkDoneMSG(senderId, solverId, duration, t());
            }
            else if(node.getUser().getLastResult() < duration)
            {
                return;
            }
            else node.getServicePrev().sendWorkDoneMSG(senderId, solverId, duration, t());
        }
    }

    @Override
    public boolean askForCurrentChainMSG(String senderId, int timeStamp) throws RemoteException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        log("B: recieved ask message from " + senderId + "\nLt: " + t());

        if(node.getServiceNext().getChain(t()) == null)
        {
            if(node.getUser().getChain() == null)
            {
                if(node.getServicePrev().getNodeId(t()).equals(node.getServiceNext().getNodeId(t())) || node.getId().equals(senderId))
                {
                    node.getUser().createChain();
                }
                else node.getServicePrev().askForCurrentChainMSG(senderId, t());
            }
            node.getServiceNext().setChain(node.getUser().getChain(), t());
            return true;
        }

        if(node.getUser().getChain() == null && node.getServiceNext().getChain(t()) != null)
        {
            node.getUser().setChain(node.getServiceNext().getChain(t()));
            node.getServicePrev().askForCurrentChainMSG(senderId, t());
            return false;
        }

        if(node.getServiceNext().getChain(t()).size() < node.getUser().getChain().size())
        {
            node.getServiceNext().setChain(node.getUser().getChain(), t());
        }
        else
        {
            if(node.getServiceNext().getChain(t()).size() == node.getUser().getChain().size())
            {
                return false;
            }
            node.getUser().setChain(node.getServiceNext().getChain(t()));
            if(!node.getId().equals(senderId)) node.getServicePrev().askForCurrentChainMSG(senderId, t());
        }

        return true;
    }

    @Override
    public boolean askForCurrentChainMSG(Chain chain, String senderId, int timeStamp) throws RemoteException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        log("B: recieved ask message from " + senderId + "\nLt: " + t());
        if(node.getId().equals(senderId))
        {
            if(chain == null)
            {
                node.getUser().createChain();
                node.getServicePrev().askForCurrentChainMSG(node.getUser().getChain(), senderId, t());
                return true;
            }
            if(node.getUser().getChain() == null || chain.size() > node.getUser().getChain().size())
            {
                node.getUser().setChain(chain);
                node.getServicePrev().askForCurrentChainMSG(chain, senderId, t());
                return true;
            }
            if(chain.size() == node.getUser().getChain().size()) return false;
        }
        if(chain == null)
        {
            node.getServicePrev().askForCurrentChainMSG(node.getUser().getChain(), senderId, t());
            return true;
        }
        if(node.getUser().getChain() == null)
        {
            node.getUser().setChain(chain);
            node.getServicePrev().askForCurrentChainMSG(chain, senderId, t());
            return true;
        }

        if(chain.size() > node.getUser().getChain().size())
        {
            node.getUser().setChain(chain);
            node.getServicePrev().askForCurrentChainMSG(chain, senderId, t());
            return false;
        }
        if(node.getUser().getChain().size() > chain.size())
        {
            node.getServicePrev().askForCurrentChainMSG(node.getUser().getChain(), senderId, t());
            return true;
        }
        node.getServicePrev().askForCurrentChainMSG(chain, senderId, t());
        return false;
    }

    @Override
    public boolean checkTerminationMSG(String senderID, int timeStamp) throws RemoteException
    {
        if(timeStamp > t()) setClock(timeStamp);
        if(node.getUser().isActive()) return false;
        if(node.getServicePrev().getNodeId(t()).equals(senderID)) return true;
        return node.getServicePrev().checkTerminationMSG(senderID,t());
    }

    @Override
    public boolean checkValidIdMSG(String senderID) throws RemoteException
    {
        if(node.getId().equals(senderID)) return false;
        if(node.getServicePrev().getNodeId(t()).equals("none")) return true;
        return node.getServicePrev().checkValidIdMSG(senderID);
    }

    @Override
    public void findWinnerMSG(String senderID, String solverId, int duration, int timeStamp) throws RemoteException
    {
        if(senderID.equals(node.getId()))
        {
            node.getUser().getPendingBlock().setSolverId(solverId);
            return;
        }
        if(duration > node.getUser().getLastResult())
        {
            node.getServicePrev().findWinnerMSG(senderID, node.getId(), node.getUser().getLastResult(), t());
        }
        else
        {
            node.getServicePrev().findWinnerMSG(senderID, solverId, duration, t());
        }

    }


    @Override
    public void setChain(Chain chain, int timeStamp) throws RemoteException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        node.getUser().setChain(chain);
    }

    @Override
    public Chain getChain(int timeStamp) throws RemoteException
    {
        if(timeStamp > t()) setClock(timeStamp);
        incrementClock();
        return node.getUser().getChain();
    }

}