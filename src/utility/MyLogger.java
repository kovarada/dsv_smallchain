package utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MyLogger
{
    private static Logger logger = null;
    private static FileHandler fh = null;
    private static boolean verbose = false;

    public static void log(String msg)
    {
        if(logger == null || fh == null)
        {
            logger = Logger.getLogger("MyLog");
            try {
                fh = new FileHandler("log\\Log.log");
            } catch (IOException  e) {
                new File("log").mkdir();
                try { fh = new FileHandler("log\\Log.log"); }
                catch (IOException ex) { ex.printStackTrace(); }
            }
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        }
        if(logger.getUseParentHandlers() != verbose) logger.setUseParentHandlers(verbose);
        logger.info(msg);
    }

    public static void setVerbose(boolean verbose) { MyLogger.verbose = verbose; }
    public static boolean getVerbose() { return verbose; }
}

