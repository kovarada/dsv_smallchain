package utility;

public class Lamport
{
    private static int clock = 0;

    public static void incrementClock() { clock++; }

    public static void setClock(int clock) { Lamport.clock = clock; }

    public static int t() {
        return clock;
    }
}
