package utility;

public class Printer
{
    public static void print(String msg) { System.out.print(msg); }

    public static void println(String msg) { System.out.println(msg); }

    public static void print_err(String msg) { System.err.println(msg); }

    public static String printCurrency(int value) { return (value + " BruhCoin" + (value != 1 ? "s" : "")); }
}
