# DSV Blockchain

Jedná s o implementaci jednoduchého distribuovaného blockchainu, ve kterém jsou
uloženy transakce mezi jednotlivými uživately. Pro validaci transakce provádí 
každý uživatel kromě autora validaci prostřednictvím proof-of-work, které
simuluje výpočet náhodně dlouhým čekáním. Uživatel, který čeká nejkratší dobu
získává odměnu a všichni si ukládájí příslušný blok. Po připojení k síti pošle
program dotaz se stavem lokálního řetězu, pokud je kretší něž ten veřejný,
řetěz se přepíše, pokud je delší řetěz lokální, rozešle se ostatním uživatelům
k aktualizaci. 

Program je napsaný v Javě a uzly spolu komunikují pomocí RMI. Z podstaty RMI
funguje každý uzel jako klient a server zároveň, aby tak vznikla relace peer-to-peer.

## Topologie

Jednotlivé uzly se připojují do kruhu, přičemž každý uzel si drží referenci na
uzel vlevo (dozadu) a vpravo (dopředu). Všechny dotazy uzel komunikuje směrem 
dozadu, dopředná reference slouží pouze ke zjišťování výpadku uzlu. Jelikož Java
RMI nezachytí odpojení, běží na uzlu samostatné vlákno které sleduje stav pravého
uzlu. Uzly jsou v síti jednoznačně určeny IP adresou a portem, pro účely blockchainu 
se uživatel přihlašuje pomocí ID, které musí být v rámci sítě unikátní.

## Terminace výpočtu

Po zveřejnění transakce sleduje autorský uzel stav ostatních uzlů, které
počítají proof-of-work a udržuje záznam nejrychlejšího výsledku. Autorský uzel opakovaně 
posílá doleva dotaz ohledně stavu pracujících uzlů. Pokud je dotazovaný uzel 
ve stavu `active`, tedy ještě pracuje, vrátí hodnotu `false`. Pokud není, přepošle 
dotaz "rekurzivně" dalšímu uzlu, pokud se tento dotaz dostane až k autorovi, 
znamená to, že výpočet skončil.

V případě odpojení autorského uzlu nebo výpadku všech pracujících uzlů se
po ukončení výpočtu žádný blok nepřipojí. Výpadek samostatného pracujícího uzlu 
nemá na výpočet vliv. Pokud se zadá transakce během výpočtu, program vyčká až se
publikuje předchozí až poté ji publikuje.

## Spuštění

Jar soubor se spustí příkazem `java -jar Blockchain.jar <params>`

**Parametry:** Všechny parametry jsou volitelné a případné hodnoty si program vyžádá.

*  `-p <port>` číslo portu

*  `-ip <address>` adresa rmi serveru
  
*  `-id <id>` id uživatele

*  `-a <address:port>` adresa libovolného uzlu v síti, pokud není zadána, program běží jako samostatná topologie

*  `-t` server bude běžet na localhost

*  `-d` server použije adresu z InetAddress `getLocalHost()`

**Ovládání** se vypíše v programu příkazem `h` nebo `help`.